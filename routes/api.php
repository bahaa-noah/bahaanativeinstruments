<?php

Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1' ] , function() {
    
    Route::get('/products'                  , 'ProductsController@index');
    Route::post('/auth'                     , 'AuthController@login');
    Route::get('/user'                      , 'UserController@index')->middleware('api_auth');
    Route::get('/user/products'             , 'ProductsController@userProducts')->middleware('api_auth');
    Route::post('/user/products'            , 'ProductsController@purchaseProduct')->middleware('api_auth');
    Route::delete('/user/products/{sku}'    , 'ProductsController@removePurchasedProduct')->middleware('api_auth');
    
});
