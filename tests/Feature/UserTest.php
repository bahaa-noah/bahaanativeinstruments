<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    
    /** @test */
    public function authenticated_user_can_see_name()
    {
        $this->withoutExceptionHandling();

        $data = ['name' => 'bahaa' ,'email' => 'bahaa2@nativeinstruments.com', 'password' =>'secret'];

        $user = factory(User::class)->create($data);
        $token = base64_encode($data['email'].":".$data['password']);
        $headers = ['Authorization' => "Basic $token"];

        $this->json('GET', 'api/v1/user', [],$headers)
            ->assertStatus(200)
            ->assertJsonStructure(['data' => ['name']])
            ->assertJson(['data' => ['name' => $user->name ]]);
    }
}
