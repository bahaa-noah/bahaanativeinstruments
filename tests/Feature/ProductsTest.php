<?php

namespace Tests\Feature;

use App\Helpers\Formatter;
use App\Helpers\Parser;
use App\Product;
use App\Purchase;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function products_listing()
    {   

        $this->get('api/v1/products')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data'=>[
                '*' => [
                    'name',
                    'sku',
                ]
                ],
                 "pagination"=> [
                        "total_count",
                        "total_pages",
                        "current_page",
                        "limit",
                        "next_page_url",
                        "prev_page_url"
                    ]
            ]);
       
    }

    /** @test */
    public function only_authenticated_users_can_see_thier_products()
    {

        $user = factory(User::class)->create();

        $token = base64_encode($user->email.":"."secret");
        $headers = ['Authorization' => "Basic $token"];

        $product        = factory(Product::class)->create();

        $purchase       = factory(Purchase::class)->create(['user_id' =>$user->id , 'product_sku' =>$product->sku ]);

        $this->json('GET', 'api/v1/user/products', [],$headers)
            ->assertStatus(200)
            ->assertJson([
                    "data"=> [[
                            "sku"=> $product->sku,
                            "name"=>$product->name
                        ]]]);
    }


    /** @test */
    public function only_authenticated_user_can_purchase_products()
    {
        
        $user = factory(User::class)->create();

        $token = base64_encode($user->email.":"."secret");
        $headers = ['Authorization' => "Basic $token"];

        $product = factory(Product::class)->create();

        $payload = ['sku' => $product->sku];

        $this->json('POST', 'api/v1/user/products', $payload ,$headers)
            ->assertStatus(201)
            ->assertJson([
                    "success" => [
                        "message" =>  "Product purchased successfully",
                        "status_code" => 201
                    ]
                ]);
    }

    /** @test */
    public function user_can_remove_purchased_product()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();

        $token = base64_encode($user->email.":"."secret");
        $headers = ['Authorization' => "Basic $token"];

        $product = factory(Product::class)->create();

        $this->json('DELETE', 'api/v1/user/products/'.$product->sku, [] ,$headers)
            ->assertStatus(200)
            ->assertJson([
                    "success" => [
                        "message" =>  "Removed successfully",
                        "status_code" => 200
                    ]
                ]);
    }

}
