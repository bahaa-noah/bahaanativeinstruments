<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    

    /** @test */
    public function auth_requires_an_email()
    {
        $this->json('POST', 'api/v1/auth')
            ->assertStatus(422)
            ->assertJson(["error"=> [
                            "message"=> [
                                "email"=> ["The email field is required."]
                            ],
                "status_code"=> 422
            ]
        ]);
    }


    /** @test */
    public function auth_requires_a_password()
    {
        $this->json('POST', 'api/v1/auth')
            ->assertStatus(422)
            ->assertJson(["error"=> [
                            "message"=> [
                                "password"=> ["The password field is required."]
                            ],
                "status_code"=> 422
            ]
        ]);

    }

    /** @test */
    public function auth_with_a_wrong_credentials()
    {

        $wrong_user_credentials = ['email' => 'test@gmail.com', 'password' => 'pppppp'];

        $this->json('POST', 'api/v1/auth', $wrong_user_credentials)
            ->assertStatus(401)
            ->assertJson(["error"=> [
                    "message"=> "Please check your credentials",
                    "status_code" => 401
                    ]
            ]);
    }

    /** @test */
    public function a_user_can_auth_successfully()
    {
        $this->withoutExceptionHandling();

        $data = ['name' => 'bahaa' ,'email' => 'bahaa@nativeinstruments.com', 'password' =>'secret'];

        $user = factory(User::class)->create($data);

        $this->json('POST', 'api/v1/auth', $data)
            ->assertStatus(200)
            ->assertJsonStructure(['data' => ['name']])
            ->assertJson(['data' => ['name' => $user->name ]]);

    }

   
}
