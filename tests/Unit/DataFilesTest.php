<?php

namespace Tests\Unit;

use App\Helpers\Parser;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DataFilesTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function the_data_files_exists()
    {
        $path =  base_path("public/data/*.csv");
        $filesArray = ['users', 'products', 'purchased'];
        $filesInFolder = [];

        foreach (glob($path) as $file) {
            $filesInFolder[] = pathinfo($file)['filename'];
        }

        $this->assertEquals(sort($filesInFolder), sort($filesArray));

    }

    /** @test */
    public function can_parse_csv_file()
    {
        $productsFilePath = base_path('public/data/products.csv');

        $parser =  new Parser;
        $parser->setPath($productsFilePath);
        
        $typeOfParsedData = gettype($parser->parseCsv());

        $this->assertEquals($typeOfParsedData,"array");
    }

}
