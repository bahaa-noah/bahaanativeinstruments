#** Bahaa NativeInstruments Products Service**

# Approach

I created Simple products service system using [laravel verion 5.7](https://laravel.com/docs/5.7).

I focused completing the task by using a TDD approach that uses the phpunit with some feature tests and other unit test. I thought that by using a TDD approach, I can identify the sceanarios before making the actual code to perform the conversion. This ensures that before coding, I understand the requirements fully as a test case with pseudo code will be written beforehand.

I have made the user authetication with just simple basic auth but I am usually using [JWT](https://jwt.io/)

I created the file import functionality in app/Helpers folder and the importing action will be done when seeding the database. 

AS for code structure I have used [Template method design pattern](https://designpatternsphp.readthedocs.io/en/latest/Behavioral/TemplateMethod/README.html) for transforming all api responses to make it consistant all over the app.

Note: I didn't write all possible test cases for the app.

After the test cases were written, I started writing the validation code first.

Finally, after writing all functionality of the code and running it against the test cases and passing.

# Assumptions
##using Docker
[Docker Installed](https://docs.docker.com/get-started/)

* unzip the project archive.
* cd to the project directory.
* run command >> docker-compose up -d
* run command >> docker-compose exec app composer update
* run command >> docker-compose exec app chmod -R 777 storage
* run command >> docker-compose exec app php artisan key:generate
* run command >> docker-compose exec app php artisan config:cache
* visit http://your_server_ip to make sure it works'

####for creating the database and mysql user 
* run command >> docker-compose exec db bash
Inside the container, log into the MySQL root administrative account:
* run commmand >> mysql -u root -p
* run commmand >> GRANT ALL ON nativeinstruments.* TO 'bahaa'@'%' IDENTIFIED BY 'password@123';
* run command  >> FLUSH PRIVILEGES;
* run command  >> exit;
* run command  >> exit;

####for creating the database tables and importing data from files
* run command >> docker-compose exec app php artisan migrate --seed

##Now you can access all the api requests through your server ip following documentation below:
[API DOCUMENTATION ON POSTMAN](https://documenter.getpostman.com/view/3752380/SVYqNdut?version=latest)

[API DOCUMENTATION ON GOOGLE DRIVE](https://docs.google.com/document/d/1BcMlrkQi7yPpc67cj5vm5pWRwG4TwgZjuRe_N2orbE4/edit?usp=sharing)

####for running tests
* run command >> docker-compose exec app vendor/bin/phpunit




##Using normal server
PHP will be installed on the users computer.
[Composer Installed](https://getcomposer.org/download/)

## Installation
*  ### Server requirements
    - PHP >= 7.1.3
    - OpenSSL PHP Extension
    - PDO PHP Extension
    - Mbstring PHP Extension
    - Tokenizer PHP Extension
    - XML PHP Extension

* cd to your server directory and pull the project :  https://bahaa-noah@bitbucket.org/bahaa-noah/bahaanativeinstruments.git
* cd to the project directory and run > composer update
* run > cp .example.env .env
* create an empty database and set the name of the database with credentials in the .env file
* run > chmod -R 777 storage
* run > php artisan key:generate
* run > php artisan config:cache 
* run > php artisan migrate
* run > php artisan db:seed


## Run tests
*please make sure sqlite is installed and running on computer or define antoher database configuration on phpunit.xml in root directory
 >> vendor/bin/phpunit
