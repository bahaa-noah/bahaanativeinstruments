<?php

use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Product::class, function (Faker $faker) {
    $name = $faker->sentence;
     return [
        'sku'   => Str::slug($name),
        'name'  => $name
    ];
});
