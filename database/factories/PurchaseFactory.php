<?php

use App\Purchase;
use Faker\Generator as Faker;

$factory->define(Purchase::class, function (Faker $faker) {
     return [
        'user_id' => function(){
            return factory(App\User::class)->create()->sku;
        },
        'product_sku' => function(){
            return factory(App\Product::class)->create()->sku;
        }
    ];
});
