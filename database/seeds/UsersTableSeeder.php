<?php

use App\Helpers\Formatter;
use App\Helpers\Parser;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    public $parser;
    public $formatter;

    public function __construct(Parser $parser, Formatter $formatter)
    {
        $this->parser       = $parser;        
        $this->formatter    = $formatter;        
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productsFilePath = base_path('public/data/users.csv');

        $this->parser->setPath($productsFilePath);
        $data = $this->parser->parseCsv();

        $this->formatter->setData($data);
        $formatedUsers = $this->formatter->formateCsv();

        foreach ($formatedUsers as $user) {
            User::create(['name' => $user[1] , 'email' => $user[2], 'password' => $user[3]]);
            dump('inserted User '. $user[1]. ' into database');
        }


    }
}
