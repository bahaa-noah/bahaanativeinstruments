<?php

use App\Helpers\Formatter;
use App\Helpers\Parser;
use App\Purchase;
use Illuminate\Database\Seeder;

class PurchesesTableSeeder extends Seeder
{

    public $parser;
    public $formatter;

    public function __construct(Parser $parser, Formatter $formatter)
    {
        $this->parser       = $parser;        
        $this->formatter    = $formatter;        
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productsFilePath = base_path('public/data/purchased.csv');

        $this->parser->setPath($productsFilePath);
        $data = $this->parser->parseCsv();

        $this->formatter->setData($data);
        $formatedPuchases = $this->formatter->formateCsv();

        foreach ($formatedPuchases as $purchase) {
            Purchase::create(['user_id' => $purchase[0],'product_sku' => $purchase[1]]);
            dump('inserted Pruchase with sku '. $purchase[1]. ' into database');
        }


    }
}
