<?php

use Illuminate\Database\Seeder;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DatabaseSeeder extends Seeder
{
    use RefreshDatabase;
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        dump('Start importing products');
        dump('==========================');
        $this->call(ProductsTableSeeder::class);

        dump('Start importing Users');
        dump('==========================');
        $this->call(UsersTableSeeder::class);

        dump('Start importing Purcheses');
        dump('==========================');
        $this->call(PurchesesTableSeeder::class);
    }
}
