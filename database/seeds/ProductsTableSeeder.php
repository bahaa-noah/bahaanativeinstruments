<?php

use App\Helpers\Formatter;
use App\Helpers\Parser;
use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    public $parser;
    public $formatter;

    public function __construct(Parser $parser, Formatter $formatter)
    {
        $this->parser       = $parser;        
        $this->formatter    = $formatter;        
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productsFilePath = base_path('public/data/products.csv');

        $this->parser->setPath($productsFilePath);
        $data = $this->parser->parseCsv();

        $this->formatter->setData($data);
        $formatedProducts = $this->formatter->formateCsv();

        foreach ($formatedProducts as $product) {
            Product::create(['sku' => $product[0] , 'name' => $product[1]]);
            dump('inserted Product '. $product[1]. ' into database');
        }


    }
}
