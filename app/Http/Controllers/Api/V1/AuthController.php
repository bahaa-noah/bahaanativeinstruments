<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\BaseController as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{


    public function login( Request $request )
    {

        $validator = Validator::make( $request->all(), [
            'email'         => 'required|string|email',
            'password'      => 'required|string|min:6|max:20',
        ]);
        
        if ($validator->fails()) {
            return $this->setStatusCode(422)->respondWithError($validator->messages());
        }

        return auth()->attempt($request->only(['email','password'])) ? 
                                $this->respond(['data'=> ['name' => auth()->user()->name ] ]) : 
                                                    $this->respondUnauthorized('Please check your credentials') ;


    }


}
