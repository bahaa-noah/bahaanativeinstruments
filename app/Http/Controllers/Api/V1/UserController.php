<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\BaseController as Controller;


class UserController extends Controller
{
    

    public function index()
    {
       return  $this->respond(['data'=> ['name' => auth()->user()->name ] ]);
    }
}
