<?php

namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Api\V1\BaseController as Controller;
use App\Product;
use App\Purchase;
use App\Transformers\ProductTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    

    protected $productTransformer;

    function __construct(ProductTransformer $productTransformer)
    {
        $this->productTransformer = $productTransformer;
    }


    public function index( Request $request )
    {
        if ($request->limit) {
            $this->setPaignation($request->limit);
        }

        $pagination = Product::paginate($this->getPagination());
        $products  = $this->productTransformer->transformCollection(collect($pagination->items()));
        return $this->respondWithPagination($pagination,['data' => $products]);
    }


    public function userProducts()
    {
        $products  = $this->productTransformer->transformCollection(auth()->user()->products);
        return $this->respond(['data' => $products]);

    }

    public function purchaseProduct(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'sku'   => 'required|string|exists:products,sku',
        ]);

        if ($validator->fails()) {
            return $this->setStatusCode(422)->respondWithError($validator->messages());
        }

        Purchase::firstOrCreate(['user_id' =>  auth()->user()->id , 'product_sku' => $request->sku]);

        return $this->respondCreated('Product purchased successfully');
    }


    public function removePurchasedProduct( Request $request )
    {
        $validator = Validator::make( ['sku' =>  $request->sku ], [
            'sku'   => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->setStatusCode(422)->respondWithError($validator->messages());
        }

        Purchase::where(['user_id' =>  auth()->user()->id , 'product_sku' => $request->sku])->delete();

        return $this->respondWithSuccess('Removed successfully');

    }

}
