<?php

namespace App\Http\Middleware;

use App\Traits\Restable;
use Closure;

class ApiAuthentication
{
    use Restable;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $credentials = [];
        if ($request->header('authorization')) {
            $data       = explode(':',base64_decode(explode(' ',$request->header('authorization'))[1]));
            $credentials['email']      = $data[0];
            $credentials['password']   = $data[1];
            
            if (auth()->attempt($credentials)) {
                return $next($request);
            }
        }
        return $this->respondUnauthorized('Please check your credentials');

    }
}
