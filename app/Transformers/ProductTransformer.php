<?php

namespace App\Transformers;
use App\Transformers\BaseTransformer as Transformer;


class ProductTransformer extends Transformer
{

    public function transform( $product ) : array
    {
        return[
            'sku'   => $product->sku,
            'name'  => $product->name
        ];
    }

}