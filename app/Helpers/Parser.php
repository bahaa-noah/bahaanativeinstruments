<?php

namespace App\Helpers;


class Parser
{
    public $filePath;


    public function parseCsv()
    {
        //turn into array
        $file = file($this->getPath());
        // remove first line
        $data = array_slice($file, 1);
        return $data;
    }

    public function setPath($filePath)
    {
        $this->filePath = $filePath;
    }

    private function getPath()
    {
        return $this->filePath;
    }


}