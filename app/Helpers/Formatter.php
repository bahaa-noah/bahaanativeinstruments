<?php

namespace App\Helpers;

use App\Helpers\Parser;


class Formatter
{

    public function setData($data)
    {
        $this->data = $data;
    }

    private function getData()
    {
        return $this->data;
    }
    

    public function FormateCsv()
    {
        return array_map('str_getcsv', $this->data);
    }





}